
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Darkside-PC
 */
public class hashload implements dbimpl{
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        hashload load = new hashload();
        
        // calculate load time
        long startTime = System.currentTimeMillis();
        System.out.println("Program Started. Now Loading Hash Index File...");
        load.readArguments(args);
        long endTime = System.currentTimeMillis();
        
        System.out.println("Load time: " + (endTime - startTime) + "ms");
    }
    
    // reading command line arguments
    @Override
    public void readArguments(String args[])
    {
        if (args.length == 1)
        {
            if (isInteger(args[0]))
            {
                createHash(Integer.parseInt(args[0]));
            }
        }
        else
        {
            System.out.println("Error: only pass in one arguments");
        }
    }
    
    // check if pagesize is a valid integer
    @Override
    public boolean isInteger(String s)
    {
        boolean isValidInt = false;
        try
        {
            Integer.parseInt(s);
            isValidInt = true;
        }
        catch (NumberFormatException e)
        {
            e.printStackTrace();
        }
        return isValidInt;
    }
    
    //reusing code from sample solution
    public void createHash(int pageSize){
        int intSize = 4;
        byte[] hashTable = new byte[idelBucketNum * intSize];
        byte[] emptyArray = new byte[intSize];
        int recCount = 0;
        int pageCount = 0;
        int recordLen = 0;
        int rid = 0;
        boolean isNextPage = true;
        boolean isNextRecord = true;
        
        FileInputStream fis;
        FileOutputStream fos;
        try {
            //initialize File Input Stream
            fis = new FileInputStream(new File(HEAP_FNAME + pageSize));
            fos = new FileOutputStream(new File(HASH_FNAME + pageSize));
            
            while (isNextPage)
            {
                byte[] bPage = new byte[pageSize];
                byte[] bPageNum = new byte[intSize];
                byte[] bucket = new byte[intSize];
                fis.read(bPage, 0, pageSize);
                System.arraycopy(bPage, bPage.length-intSize, bPageNum, 0, intSize);
                int pageNum = ByteBuffer.wrap(bPageNum).getInt();
                
                // reading by record, return true to read the next record
                isNextRecord = true;
                while (isNextRecord)
                {
                    byte[] bRecord = new byte[RECORD_SIZE];
                    byte[] bRid = new byte[intSize];
                    try
                    {
                        System.arraycopy(bPage, recordLen, bRecord, 0, RECORD_SIZE);
                        System.arraycopy(bRecord, 0, bRid, 0, intSize);
                        rid = ByteBuffer.wrap(bRid).getInt();
                        if (rid != recCount)
                        {
                            isNextRecord = false;
                        }
                        else
                        {
                            byte[] offsetByte;
                            String record = new String(bRecord);
                            String BN_NAME = record
                                    .substring(RID_SIZE+REGISTER_NAME_SIZE,
                                            RID_SIZE+REGISTER_NAME_SIZE+BN_NAME_SIZE).trim();
                            //Calculate hashindex 
                            int hashIndex = hashfunction.hash(BN_NAME, idelBucketNum);
                            //get offset index in the hashindex
                            int offset = (pageNum * pageSize) + rid * RECORD_SIZE;
                            offsetByte = intToByteArray(offset);
                            
                            while(true){
                                System.arraycopy(hashTable, hashIndex * intSize, bucket, 0, intSize);
                                //check if bucket is empty if not do linear probing
                                if(!Arrays.equals(bucket, emptyArray)){
                                    hashIndex += intSize;
                                }
                                else{
                                    System.arraycopy(offsetByte, 0, hashTable, hashIndex * intSize, intSize);
                                    break;
                                }
                            }
                            //increment record offset
                            recordLen += RECORD_SIZE;
                        }
                        recCount++;
                        // if recordLen exceeds pagesize, catch this to reset to next page
                    }
                    catch (ArrayIndexOutOfBoundsException e)
                    {
                        isNextRecord = false;
                        recordLen = 0;
                        recCount = 0;
                        rid = 0;
                    }
                }
                // check to complete all pages
                if (ByteBuffer.wrap(bPageNum).getInt() != pageCount)
                {
                    isNextPage = false;
                }
                pageCount++;
            }
            fos.write(hashTable);
            fos.flush();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    // converts ints to a byte array of allocated size using bytebuffer
    public byte[] intToByteArray(int i)
    {
        ByteBuffer bBuffer = ByteBuffer.allocate(4);
        bBuffer.putInt(i);
        return bBuffer.array();
    }
}


/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Darkside-PC
 */
public class hashquery implements dbimpl{
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        hashquery query = new hashquery();
        
        // calculate query time
        long startTime = System.currentTimeMillis();
        query.readArguments(args);
        long endTime = System.currentTimeMillis();
        
        System.out.println("Query time: " + (endTime - startTime) + "ms");
    }
    
    
    // reading command line arguments
    @Override
    public void readArguments(String args[])
    {
        if (args.length == 2)
        {
            if (isInteger(args[1]))
            {
                readHash(args[0], Integer.parseInt(args[1]));
            }
        }
        else
        {
            System.out.println("Error: only pass in two arguments");
        }
    }
    
    // check if pagesize is a valid integer
    @Override
    public boolean isInteger(String s)
    {
        boolean isValidInt = false;
        try
        {
            Integer.parseInt(s);
            isValidInt = true;
        }
        catch (NumberFormatException e)
        {
            e.printStackTrace();
        }
        return isValidInt;
    }
    //reusing code from sample solution
    public void readHash(String name, int pagesize)
    {
        File heapfile = new File(HEAP_FNAME + pagesize);
        File hashfile = new File(HASH_FNAME + pagesize);
        int intSize = 4;
        int index = 0;
        int offsetIndex;
        try
        {
            byte[] bucket = new byte[intSize];
            byte[] emptyByte = new byte[intSize];
            byte[] bRecord = new byte[RECORD_SIZE];
            index = hashfunction.hash(name, idelBucketNum);
            //Calculate position index inside the hash file
            int fileIndex = index * intSize;
            
            while(true){
                //initialize File Input Stream
                FileInputStream heapFis = new FileInputStream(heapfile);
                FileInputStream hashFis = new FileInputStream(hashfile);
                //skip to desire index to retrive offsetindex inside the hash file
                hashFis.skip(fileIndex);
                hashFis.read(bucket, 0, intSize);
                //skip to desire index to retrive record from the heap file
                offsetIndex = ByteBuffer.wrap(bucket).getInt();
                heapFis.skip(offsetIndex);
                heapFis.read(bRecord, 0, RECORD_SIZE);
                //covert byte array to String
                String record = new String(bRecord);
                String BN_NAME = record
                        .substring(RID_SIZE+REGISTER_NAME_SIZE,
                                RID_SIZE+REGISTER_NAME_SIZE+BN_NAME_SIZE).trim();
                //match the search target with hash file postion if match found-
                //print out the record else jump to next bucket 
                if(BN_NAME.equalsIgnoreCase(name)){
                    System.out.println("Record Found: "
                            + "\n----------------------------------\n" + record);
                    fileIndex += intSize;
                }
                else if(Arrays.equals(emptyByte, bucket)){
                    System.out.println("----------------------------------\n" 
                            + "End Of Result");
                    break;    
                }
                else{
                    fileIndex += intSize;
                }
                
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Files not found.");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
